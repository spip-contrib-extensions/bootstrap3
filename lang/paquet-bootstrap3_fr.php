<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	'bootstrap3_slogan' => 'BootStrap 3 pour SPIP',
	'bootstrap3_description' => 'Adaptateur BootStrap 3 pour SPIP.
Inclue hashgrid.',
);

?>